﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;
using Microsoft.Win32;

namespace IQFeeder
{
    class Program
    {       
        static void Main(string[] args)
        {
            try
            {
                System.IO.Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings["BaseLogsDirectory"]);
            }
            catch { }

            Tracer.GetTracer().Info("Starting IQFeeder");

            IQMain iq = new IQMain();

            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(iq.Start));            
            thread.Start(); 

            Console.ReadKey();
        }

        

    }
}
