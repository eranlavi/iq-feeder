﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;
using Microsoft.Win32;
using System.Xml;
using System.Collections.Concurrent;
using System.Configuration;
using System.Threading;

namespace IQFeeder
{
    public class IQMain
    {
        // global variables for socket communications to the level1 socket
        AsyncCallback m_pfnLevel1Callback;
        Socket m_sockLevel1;
        // we create the socket buffer global for performance
        byte[] m_szLevel1SocketBuffer = new byte[8096];
        // stores unprocessed data between reads from the socket
        string m_sLevel1IncompleteRecord = "";
        // flag for tracking when a call to BeginReceive needs called
        bool m_bLevel1NeedBeginReceive = true;

        bool SymbolsSubscribe = true;
        private class LastQuote
        {
            public double Bid;
            public double Ask;
        }
        ConcurrentDictionary<string, LastQuote> dLastQuotes;
        Dictionary<string, string> OutputNames;

        UnifeedServer ufs;

        private class QuoteData
        {
            public string In;
            public string Out;
            public string Bid;
            public string Ask;
        }
        //BlockingCollection<QuoteData> bcQuoteData;
        ConcurrentQueue<QuoteData> bcQuoteData;
        object locker;

        Dictionary<string, ConcurrentQueue<QuoteData>> bcThreadHash;
        Dictionary<string, ManualResetEventSlim> threadSet;
        Dictionary<string, string> symbolThreadHash;

        public IQMain()
        {
            dLastQuotes = new ConcurrentDictionary<string, LastQuote>();
            OutputNames = new Dictionary<string, string>();

            ufs = new UnifeedServer();
            ufs.Start();

            int MaxThreads = Convert.ToInt32(ConfigurationManager.AppSettings["THREADS"]);
            bcThreadHash = new Dictionary<string, ConcurrentQueue<QuoteData>>();
            threadSet = new Dictionary<string, ManualResetEventSlim>();
            for (int i = 0; i < MaxThreads; i++)
            {
                ManualResetEventSlim evt = new ManualResetEventSlim(false);
                threadSet.Add((i + 1).ToString(), evt);
            }

            for (int i = 0; i < MaxThreads; i++)
            {                
                bcQuoteData = new ConcurrentQueue<QuoteData>();
                string threadName = (i + 1).ToString();
                bcThreadHash.Add(threadName, bcQuoteData);

                Thread worker = new Thread(() => WorkerThread(bcQuoteData, (i + 1).ToString()));
                worker.Name = threadName;
                worker.Start();
                Thread.Sleep(100);
            }
        }

        private void WorkerThread(ConcurrentQueue<QuoteData> bcQuoteData, string ThradNum)
        {
            ManualResetEventSlim evt = threadSet[ThradNum];
            int BlockTime = Convert.ToInt32(ConfigurationManager.AppSettings["threadBlockTimeMSec"]);
            while(true)
            {
                //evt.WaitOne();

                QuoteData qd;
                if (!bcQuoteData.TryDequeue(out qd))
                {
                    //Thread.Sleep(BlockTime);
                    evt.Wait();
                    continue;
                }

                evt.Reset();

                double bid = double.Parse(qd.Bid);
                double ask = double.Parse(qd.Ask);

                LastQuote lq;
                if (!dLastQuotes.TryGetValue(qd.In, out lq))
                {
                    lock (locker)
                    {
                        ufs.SendData(qd.Out + " " + ToFastDateTimeString() + " " + qd.Bid + " " + qd.Ask + "\r\n");
                    }
                    lq = new LastQuote();
                    lq.Bid = bid;
                    lq.Ask = ask;
                    dLastQuotes.TryAdd(qd.In, lq);
                }
                else
                {
                    if (lq.Bid != bid || lq.Ask != ask)
                    {
                        lock (locker)
                        {
                            ufs.SendData(qd.Out + " " + ToFastDateTimeString() + " " + qd.Bid + " " + qd.Ask + "\r\n");
                        }
                        lq.Bid = bid;
                        lq.Ask = ask;
                    }
                }
            }
        }

        public void Start()
        {
            // create the socket and tell it to connect
            m_sockLevel1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress ipLocalhost = IPAddress.Parse("127.0.0.1");

            // pull the level 1 port out of the registry.  we use the Level 1 port because we want streaming updates
            int iPort = GetIQFeedPort("Level1");

            IPEndPoint ipendLocalhost = new IPEndPoint(ipLocalhost, iPort);

            try
            {
                // tell the socket to connect to IQFeed
                m_sockLevel1.Connect(ipendLocalhost);

                // Set the protocol for the level1 socket to 5.1 so we have access to the trades only watch request
                SendRequestToIQFeed("S,SET PROTOCOL,5.1\r\n");

                // this example is using asynchronous sockets to communicate with the feed.  As a result, we are using .NET's BeginReceive and EndReceive calls with a callback.
                // we call our WaitForData function (see below) to notify the socket that we are ready to receive callbacks when new data arrives
                WaitForData("Level1");
            }
            catch (Exception ex)
            {
                Tracer.GetTracer().Error("[IQMain] [Start] Exception ", ex);
            }            
        }

        /// <summary>
        /// Gets local IQFeed socket ports from the registry
        /// </summary>
        /// <param name="sPort"></param>
        /// <returns></returns>
        private int GetIQFeedPort(string sPort)
        {
            int iReturn = 0;
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\DTN\\IQFeed\\Startup");
            if (key != null)
            {
                string sData = "";
                switch (sPort)
                {
                    case "Level1":
                        // the default port for Level 1 data is 5009.
                        sData = key.GetValue("Level1Port", "5009").ToString();
                        break;
                    case "Lookup":
                        // the default port for Lookup data is 9100.
                        sData = key.GetValue("LookupPort", "9100").ToString();
                        break;
                    case "Level2":
                        // the default port for Level 2 data is 9200.
                        sData = key.GetValue("Level2Port", "9200").ToString();
                        break;
                    case "Admin":
                        // the default port for Admin data is 9300.
                        sData = key.GetValue("AdminPort", "9300").ToString();
                        break;
                    case "Derivative":
                        // the default port for derivative data is 9400
                        sData = key.GetValue("DerivativePort", "9400").ToString();
                        break;
                }
                iReturn = Convert.ToInt32(sData);
            }
            return iReturn;
        }

        private void DoSubscribe()
        {
            locker = new object();
            symbolThreadHash = new Dictionary<string, string>();
            

            int MaxThreads = Convert.ToInt32(ConfigurationManager.AppSettings["THREADS"]);
            if (MaxThreads <= 0)
                MaxThreads = 1;
            int thCounter = 1;

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("Symbols.xml");
            XmlNodeList nodes = xDoc.SelectNodes("//symbols/symbol");

            foreach (XmlNode node in nodes)
            {
                try
                {                    
                    symbolThreadHash.Add(node.Attributes["in"].Value, thCounter.ToString());
                    thCounter++;
                    if (thCounter > MaxThreads)
                        thCounter = 1;

                    OutputNames.Add(node.Attributes["in"].Value, node.Attributes["out"].Value);

                    // When you watch a symbol, you will get a snapshot of data that is currently available in the servers
                    // for that symbol.  The snapshot will include a Fundamental message followed by a Summary message and then
                    // you will continue to get Update messages anytime a field is updated until you issue an unwatch for the symbol.

                    Tracer.GetTracer().Info("[Subscribe] Symbol: " + node.Attributes["in"].Value);

                    // the command we need to send to watch a symbol is wSYMBOL\r\n
                    SendRequestToIQFeed(String.Format("w{0}\r\n", node.Attributes["in"].Value));
                }
                catch (Exception ex)
                {
                    Tracer.GetTracer().Error("[Subscribe] Exception ", ex);
                }
            }
        }

        /// <summary>
        /// Since we are using an async socket, we just tell the socket that we are ready to recieve data.
        /// The .NET framework will then call our callback (OnReceive) when there is new data to be read off the socket
        /// </summary>
        private void WaitForData(string sSocketName)
        {
            if (sSocketName.Equals("Level1"))
            {
                if (SymbolsSubscribe)
                {
                    SymbolsSubscribe = false;

                    SendRequestToIQFeed(String.Format("S,SELECT UPDATE FIELDS,{0}\r\n", "Symbol,Bid,Ask"));

                    DoSubscribe();                    
                }

                // make sure we have a callback created
                if (m_pfnLevel1Callback == null)
                {
                    m_pfnLevel1Callback = new AsyncCallback(OnReceive);
                }

                // send the notification to the socket.  It is very important that we don't call Begin Reveive more than once per call
                // to EndReceive.  As a result, we set a flag to ignore multiple calls.
                if (m_bLevel1NeedBeginReceive)
                {
                    m_bLevel1NeedBeginReceive = false;
                    // we pass in the sSocketName in the state parameter so that we can verify the socket data we receive is the data we are looking for
                    m_sockLevel1.BeginReceive(m_szLevel1SocketBuffer, 0, m_szLevel1SocketBuffer.Length, SocketFlags.None, m_pfnLevel1Callback, sSocketName);
                }
            }
        }

        /// <summary>
        /// Sends a string to the socket connected to IQFeed
        /// </summary>
        /// <param name="sCommand"></param>
        private void SendRequestToIQFeed(string sCommand)
        {
            // and we send it to the feed via the socket
            byte[] szCommand = new byte[sCommand.Length];
            szCommand = Encoding.ASCII.GetBytes(sCommand);
            int iBytesToSend = szCommand.Length;
            try
            {
                int iBytesSent = m_sockLevel1.Send(szCommand, iBytesToSend, SocketFlags.None);
                if (iBytesSent != iBytesToSend)
                {
                    Tracer.GetTracer().Error(String.Format("Error Sending Request:\r\n{0}", sCommand.TrimEnd("\r\n".ToCharArray())));                    
                }
                else
                {
                    Tracer.GetTracer().Error(String.Format("Request Sent Successfully:\r\n{0}", sCommand.TrimEnd("\r\n".ToCharArray())));                                        
                }
            }
            catch (SocketException ex)
            {
                // handle socket errors
                Tracer.GetTracer().Error(String.Format("Socket Error Sending Request:\r\n{0}\r\n{1}", sCommand.TrimEnd("\r\n".ToCharArray()), ex.Message));                
            }
        }

        /// <summary>
        /// OnReceive is our Callback that gets called by the .NET socket class when new data arrives on the socket
        /// </summary>
        /// <param name="asyn"></param>
        private void OnReceive(IAsyncResult asyn)
        {
            // first verify we received data from the correct socket.  This check isn't really necessary in this example since we 
            // only have a single socket but if we had multiple sockets, we could use this check to use the same callback to recieve data from
            // multiple sockets
            if (asyn.AsyncState.ToString().Equals("Level1"))
            {
                // read data from the socket.
                int iReceivedBytes = 0;
                iReceivedBytes = m_sockLevel1.EndReceive(asyn);
                // set our flag back to true so we can call begin receive again
                m_bLevel1NeedBeginReceive = true;
                // in this example, we will convert to a string for ease of use.
                string sData = Encoding.ASCII.GetString(m_szLevel1SocketBuffer, 0, iReceivedBytes);

                // When data is read from the socket, you can get multiple messages at a time and there is no guarantee
                // that the last message you receive will be complete.  It is possible that only half a message will be read
                // this time and you will receive the 2nd half of the message at the next call to OnReceive.
                // As a result, we need to save off any incomplete messages while processing the data and add them to the beginning
                // of the data next time.
                sData = m_sLevel1IncompleteRecord + sData;
                // clear our incomplete record string so it doesn't get processed next time too.
                m_sLevel1IncompleteRecord = "";

                // now we loop through the data breaking it appart into messages.  Each message on this port is terminated
                // with a newline character ("\n")
                string sLine = "";
                int iNewLinePos = -1;
                while (sData.Length > 0)
                {
                    iNewLinePos = sData.IndexOf("\n");
                    if (iNewLinePos > 0)
                    {
                        sLine = sData.Substring(0, iNewLinePos);
                        // we know what type of message was recieved by the first character in the message.
                        switch (sLine[0])
                        {
                            case 'Q':
                                ProcessUpdateMsg(sLine);
                                break;
                            case 'F':
                                ProcessFundamentalMsg(sLine);
                                break;
                            case 'P':
                                ProcessSummaryMsg(sLine);
                                break;
                            case 'N':
                                //ProcessNewsHeadlineMsg(sLine);
                                break;
                            case 'S':
                                ProcessSystemMsg(sLine);
                                break;
                            case 'R':
                                //ProcessRegionalMsg(sLine);
                                break;
                            case 'T':
                                //ProcessTimestamp(sLine);
                                break;
                            case 'E':
                                ProcessErrorMsg(sLine);
                                break;
                            default:
                                // we processed something else we weren't expecting.  Ignore it
                                break;
                        }
                        // move on to the next message.  This isn't very efficient but it is simple (which is the focus of this example).
                        sData = sData.Substring(sLine.Length + 1);
                    }
                    else
                    {
                        // we get here when there are no more newline characters in the data.  
                        // save off the rest of message for processing the next batch of data.
                        m_sLevel1IncompleteRecord = sData;
                        sData = string.Empty;
                    }
                }

                // call wait for data to notify the socket that we are ready to receive another callback
                WaitForData("Level1");                
            }
        }

        /// <summary>
        /// Process an update message from the feed.
        /// </summary>
        /// <param name="sLine"></param>
        private void ProcessUpdateMsg(string sLine)
        {
            // Update messages are sent to the client anytime one of the fields in the current fieldset are updated.
            // In this example, we just display the data to the user.
            // For a list of fields that are contained in the update message, please check the documentation page UpdateSummary Message Format.
            
            string[] param = sLine.Split(',');

            string name;
            if (!OutputNames.TryGetValue(param[1], out name))
                return;

            string thCounter;
            if (symbolThreadHash.TryGetValue(param[1], out thCounter))
            {
                QuoteData qd = new QuoteData();
                qd.In = param[1];
                qd.Out = name;
                qd.Bid = param[2];
                qd.Ask = param[3];
                bcThreadHash[thCounter].Enqueue(qd);
                threadSet[thCounter].Set();
            }

            return;            
        }
                
        private string ToFastDateTimeString()
        {            
            DateTime Date = DateTime.Now;
            char[] chars = new char[23];
            chars[0] = Digit(Date.Day / 10);
            chars[1] = Digit(Date.Day % 10);
            chars[2] = '/';
            chars[3] = Digit(Date.Month / 10);
            chars[4] = Digit(Date.Month % 10);
            chars[5] = '/';
            chars[6] = Digit(Date.Year / 1000);
            chars[7] = Digit(Date.Year % 1000 / 100);
            chars[8] = Digit(Date.Year % 100 / 10);
            chars[9] = Digit(Date.Year % 10);
            chars[10] = '-';
            chars[11] = Digit(Date.Hour / 10);
            chars[12] = Digit(Date.Hour % 10);
            chars[13] = ':';
            chars[14] = Digit(Date.Minute / 10);
            chars[15] = Digit(Date.Minute % 10);
            chars[16] = ':';
            chars[17] = Digit(Date.Second / 10);
            chars[18] = Digit(Date.Second % 10);
            chars[19] = '.';
            chars[20] = Digit(Date.Millisecond / 100);
            chars[21] = Digit(Date.Millisecond % 100 / 10);
            chars[22] = Digit(Date.Millisecond % 10);
            
            return new string(chars);
        }
        private char Digit(int value) { return (char)(value + '0'); }

        /// <summary>
        /// Process a system message from the feed
        /// </summary>
        /// <param name="sLine"></param>
        private void ProcessSystemMsg(string sLine)
        {
            // system messages are sent to inform the client about current system information.
            // In this example, we just display the data to the user.
            // For a list of system messages that can be sent and the fields each contains, please check the documentation page System Messages.            
            Tracer.GetTracer().Info("[System] " + sLine);
        }

        /// <summary>
        /// Process an error message from the feed
        /// </summary>
        /// <param name="sLine"></param>
        void ProcessErrorMsg(string sLine)
        {
            // Error messages are sent to the client to inform the client of problems.
            // In this example, we just display the data to the user.            
            Tracer.GetTracer().Info("[Error] " + sLine);
        }

        /// <summary>
        /// Process a summary message from the feed.
        /// </summary>
        /// <param name="sLine"></param>
        void ProcessSummaryMsg(string sLine)
        {
            // summary data will be in the same format as the Update messages and will contain the most recent data for each field at the 
            //      time you watch the symbol.
            // In this example, we just display the data to the user.
            // For a list of fields that are contained in the Summary message, please check the documentation page UpdateSummaryMessage Format.            
            Tracer.GetTracer().Info("[Summary] " + sLine);
        }

        /// <summary>
        /// Process a fundamental message from the feed
        /// </summary>
        /// <param name="sLine"></param>
        private void ProcessFundamentalMsg(string sLine)
        {
            // fundamental data will contain data about the stock symbol that does not frequently change (at most once a day).
            // In this example, we just display the data to the user.
            // For a list of fields that are contained in the fundamental message, please check the documentation page Fundamental Message Format.            
            Tracer.GetTracer().Info("[Fundamental] " + sLine);
        }



    }
}
